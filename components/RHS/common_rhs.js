import AdsCode from '../dpf/adsCode';

function RHS(){
    return(
		<div id="right">
			<AdsCode 
				ads_div = ''
				ads_code = "ATF_300"
				ads_class = "right-adbox"
			/>
			<div className="vspacer20"></div>
		
			{	/*<!------Corona-Warriors-Banner-------------->*/}
			<div className="vspacer20"></div>
			<a href="https://www.news18.com/corona-warriors/" target="_blank">
				<img src="https://images.news18.com/static_news18/pix/ibnhome/news18/images/corona-warriors-300X100_N1.gif" alt="Corona Worriors" title="Corona Worriors" />
			</a> 
			{/*<!------End Corona-Warriors-Banner-------------->*/}

			<AdsCode 
				ads_div = ''
				ads_code = "MTF_300"
				ads_class = "right-adbox"
			/>

			<div className="stck-search vspacer20">
				<div className="sinner">
					<div className="market_update">
						<a href="http://www.news18.com/stocks/indian-stocks-market-live/?utm_campaign=Market_Watch" id="market_update_anc"></a>
					</div>
					<div className="searchbox">
						<form action="https://secure.news18.com/stocks/search-result-post-submission.php" method="get" id="search_bx_frm">
							<div id="autocomplete-container">  
								<input type="text" placeholder="Search Stock" name="stock_serach" id="stock_serach" className="ui-autocomplete-input"/> <a href="#" className="searchicon"></a>
								<ul id="autocomplete-results"></ul>
							</div>
						</form>
					</div>
				</div>
			</div>
	
			<div className="vspacer20"></div>
			<link rel="stylesheet" href="https://images.news18.com/static_news18/css/revamp/mission_paani_pledge.min.css"/>
			<div className="pani-wid">
				<a href="https://www.news18.com/mission-paani/">
					<img className="lazy" src="https://images.news18.com/ibnlive/uploads/2019/08/news18.gif?impolicy=website&amp;width=300&amp;height=190" data-src="https://images.news18.com/static_news18/pix/ibnhome/news18/images/mission_panni_banner.jpg" data-srcset="https://images.news18.com/static_news18/pix/ibnhome/news18/images/mission_panni_banner.jpg 1x" alt="Mission Paani"/>
				</a>
	
				<div className="paani-footer">
					<img className="lazy" src="https://images.news18.com/ibnlive/uploads/2019/08/news18.gif?impolicy=website&amp;width=300&amp;height=190" data-src="https://images.news18.com/static_news18/pix/ibnhome/news18/mission-paani/pani-footer.jpg" data-srcset="https://images.news18.com/static_news18/pix/ibnhome/news18/mission-paani/pani-footer.jpg 1x"/>
					<div className="time-counts">
						<span id="div_counter_5">7</span><span id="div_counter_4">7</span><span id="div_counter_3">4</span><span id="div_counter_2">3</span><span id="div_counter_1">0</span>
					</div>
				</div>
			</div>	
							
			<AdsCode 
				ads_div = ''
				ads_code = "BTF_300"
				ads_class = "right-adbox"
			/>
  		</div>
    )
}

export default RHS;