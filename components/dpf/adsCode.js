import AdList from "./adsList.json";
import AdsDimension from "./adsDimension.json";

const AD_UNIT_PATH = '/1039154/NW18_ENG_Desktop/NW18_ENG_Home/NW18_ENG_Home_Home/NW18_ENG_HOM_ATF_728';

class AdsCode extends React.Component {

    constructor(props){
        super(props);
        // console.log(props);
        this.state = {
            ads_div: AdList.homepage.ads_list[props.ads_code],
            ads_dim: AdsDimension.homepage.ads_dim[props.ads_code],
            ads_code: AdList.homepage.ads_list[props.ads_code]
        };
    }

    adCodes(adsDiv, adsDim, adsCode){
        return (
            googletag.cmd.push(function() {
                googletag.defineSlot('/1039154/'+adsCode, adsDim, adsDiv).addService(googletag.pubads());
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
                googletag.display(adsDiv);
            })
        );
    }

    componentDidMount() {
        if(this.props.ads_div == ""){
            this.adCodes(AdList.homepage.ads_list[this.props.ads_code], AdsDimension.homepage.ads_dim[this.props.ads_code], AdList.homepage.ads_list[this.props.ads_code]);
        }else{
            this.adCodes(this.props.ads_div, AdList.homepage.ads_list[this.props.ads_code]);
        }
    }

    render() {
        return (
            <>
                <div id={this.state.ads_div} className ={this.props.ads_class} />
            </>
        );
    }
}

export default AdsCode;