function footer(){
	return(
		<div>
			<div className="outer ftbox">	
				<h3 className="ftrlvlt-hd">Live TV
				<img src="https://images.news18.com/static_news18/pix/ibnhome/news18/images/desktop/farrow.png" alt=""/></h3>	
				<div className="clearfix ftrchnl">
					<a href="#" className="ftrchnl-ar chnlarl slick-arrow" aria-disabled="false"></a>
					<a href="#" className="ftrchnl-ar chnlarr slick-arrow slick-disabled" aria-disabled="true" > </a>  
				</div>
			</div>	
			<div className="fmid">
				<div className="outer df">       
					<div className="topic">     
						<h2>sections</h2>     
						<ul><li><a href="/india/">India</a></li>
							<li><a href="/lifestyle/">Lifestyle</a></li>				
							<li><a href="/tech/">Tech</a></li>
							<li><a href="/politics/">Politics</a></li>
							<li><a href="/auto/">Auto</a></li>
							<li><a href="/sports/">Sports</a></li>
							<li><a href="/ivideos/">Videos</a></li>
							<li><a href="/football/">Football</a></li>
							<li><a href="https://www.news18.com/press-release/">Press Release</a></li>
						</ul>            
					</div>
					<div className="fletest-news">   
						<h2>Latest News</h2>        
						<ul>
							<li><a href="https://www.news18.com/news/india/coronavirus-vaccine-by-early-november-adar-poonawala-tells-naveen-patnaik-2728311.html">Coronavirus Vaccine by Early November, Adar Poonawala tells Naveen Patnaik</a></li>
							<li><a href="https://www.news18.com/news/auto/air-india-cuts-employee-allowance-by-up-to-50-per-cent-revised-allowances-effective-april-1-2728329.html">Air India Cuts Employee Allowance by up to 50 Per Cent, Revised Allowances Effective April 1</a></li>
							<li><a href="https://www.news18.com/news/education-career/upsc-to-release-notification-for-combined-medical-services-exam-2020-on-july-29-2728321.html">UPSC to Release Notification for Combined Medical Services Exam 2020 on July 29</a></li>
							<li><a href="https://www.news18.com/news/india/all-11-former-cops-guilty-of-killing-bharatpur-royal-scion-given-life-terms-2728307.html">All 11 Former Cops Guilty of Killing Bharatpur Royal Scion Given Life Terms</a></li>
							<li><a href="https://www.news18.com/news/movies/bigg-boss-14-onir-clueless-about-being-one-of-the-contestants-2728213.html">Bigg Boss 14: Onir Clueless About Being 'One Of the Contestants'</a></li>
						</ul>     
					</div> 
					<div className="topic fleft fornwftr"> 
						<ul> 
							<li><a href="/about_us.php">About Us </a></li> 
							<li><a href="/privacy_policy.html">Privacy Policy </a></li>
							<li><a href="/cookie_policy.html">Cookie Policy </a></li>
							<li><a href="/disclaimer.php">Disclaimer </a></li>
							<li><a href="/contact_us.php">Contact Us  </a></li>
							<li><a href="/rss/">Sitemap </a></li>
							<li><a href="/complaint.php">Complaint Redressal </a></li>
							<li><a href="/advertisement_with_us.php">Advertise With Us</a>
						</li></ul> 
					</div>		
				</div> 
				<div className="from-network">
					<div className="outer">      
						<h3>Network 18 Sites</h3>  
						<ul>         
							<li><a href="https://hindi.news18.com" target="_blank">News18 India </a></li>
							<li><a href="/cricketnext/">CricketNext  </a></li>
							<li><a href="https://bengali.news18.com/" target="_blank">Bangla News </a></li>
							<li><a href="https://gujarati.news18.com/" target="_blank">Gujarati News </a></li>
							<li><a href="https://urdu.news18.com/" target="_blank">Urdu News </a></li>
							<li><a href="https://lokmat.news18.com/" target="_blank">Marathi News </a></li>
							<li><a href="https://www.topperlearning.com/" target="_blank">TopperLearning </a></li>
							<li><a href="https://www.moneycontrol.com/" target="_blank">Moneycontrol  </a></li>
							<li><a href="https://www.firstpost.com/" target="_blank">Firstpost  </a></li>
							<li><a href="https://compareindia.news18.com/" target="_blank">CompareIndia </a></li>
							<li><a href="https://www.cnbctv18.com/" target="_blank">CNBCTV18 </a></li>
							<li><a href="https://www.historyindia.com/" target="_blank">History India </a></li>
							<li><a href="https://www.mtvindia.com/" target="_blank">MTV India </a></li>
							<li><a href="https://www.in.com/" target="_blank">In.com </a></li>
							<li><a href="https://www.topperlearning.com/doubts-solutions/all-questions/" target="_blank">Clear Study Doubts</a></li>
							<li><a href="https://www.topperlearning.com/become-partner" target="_blank">Education Franchisee Opportunity</a></li>
							<li><a href="https://www.caprep18.com/" target="_blank">CAprep18</a></li>
						</ul>          
					</div>      
				</div>
				<div className="fbottom-txt">
					<div className="outer">CNN name, logo and all associated elements ® and © 2020 Cable News Network LP, LLLP. A Time Warner Company. All rights reserved. CNN and the CNN logo are registered marks of Cable News Network, LP LLLP, displayed with permission. Use of the CNN name and/or logo on or as part of NEWS18.com does not derogate from the intellectual property rights of Cable News Network in respect of them. © Copyright Network18 Media and Investments Ltd 2020. All rights reserved.</div>
				</div>  
			</div>
		</div>
	)
}

export default footer;