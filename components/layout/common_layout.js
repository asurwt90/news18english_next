// import Link from 'next/link'
import Head from 'next/head'
import Header from '../header/Header'
import Footer from '../footer/Footer'
import RHS from '../RHS/common_rhs'
import Metatag from '../header/Metatag'
import AdsCode from '../dpf/adsCode';

const Layout = (props) => (
    <>
      <Head>
        <Metatag title={props.title} />
        <script async src="http://securepubads.g.doubleclick.net/tag/js/gpt.js" type="text/javascript"></script>
        <link rel="stylesheet" href="https://images.news18.com/static_news18/pix/ibnhome/news18/css/header/common_header.css?v=2" />
        <link rel="stylesheet" href="https://images.news18.com/static_news18/pix/ibnhome/news18/css/desktop/app_home_desktop.css" />
        <link rel="stylesheet" href="https://images.news18.com/static_news18/pix/ibnhome/news18/css/desktop/revamp_article-new_min.css?v=1.3" />
        <script
            dangerouslySetInnerHTML={{
                __html: `
                    var googletag = googletag || {};
                    googletag.cmd = googletag.cmd || [];
                `
            }}
        />
      </Head>
      <AdsCode 
        ads_div = ""
        ads_code = "ATF_728"
        ads_class = "n18thder"
      />
      <header>
          <Header/>
      </header>
      <div className="body-container" id="true">
        <div className="justify-content flex-box">
          {props.children}
          <RHS/> 
        </div>
      </div>
      <footer>
        <Footer/>
      </footer>
    </>
);

export default Layout;