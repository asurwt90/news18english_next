import { useRouter } from 'next/router'

import Menul1 from './menu_l1';

function Header() {
	return (
		<div>		
			<div className="n18bhdr">
				<div className="n18hcontainer">
					<div className="logonsection">
						<a href="/" className="nhlogo">
							<img src="https://images.news18.com/static_news18/pix/ibnhome/news18/megamenu/News18_logo.svg" alt="News18 Logo" />
						</a>		
					</div>
					<div className="nhhdr-nav">
						<div className="lnlivetv">
							<div className="languagebox">
								<span>CHANGE LANGUAGE</span>
								<div className="linner"><a href="https://www.news18.com/">English <span className="nhlanguate-arrow hsocial-sprite"></span></a>
									<div className="lddnav">
										<a href="https://hindi.news18.com/" target="_blank">हिन्दी </a>
										<a href="https://bengali.news18.com/" target="_blank">বাংলা </a>
										<a href="https://lokmat.news18.com/" target="_blank">मराठी  </a>
										<a href="https://gujarati.news18.com/" target="_blank">ગુજરાતી  </a>
										<a href="https://kannada.news18.com/" target="_blank">ಕನ್ನಡ  </a>
										<a href="https://tamil.news18.com/" target="_blank">தமிழ்  </a>
										<a href="https://malayalam.news18.com/" target="_blank">മലയാളം </a>
										<a href="https://telugu.news18.com/" target="_blank">తెలుగు </a>
										<a href="https://punjab.news18.com/" target="_blank">ਪੰਜਾਬੀ </a>
										<a href="https://urdu.news18.com/" target="_blank">اردو  </a>
										<a href="https://assam.news18.com/" target="_blank">অসমীয়া  </a>
										<a href="https://odia.news18.com/" target="_blank">ଓଡ଼ିଆ </a>
									</div>
								</div>
							</div>
							<div className="nhlivetv"><a href="/livetv/"><span className="nhlivetv-icon hsocial-sprite"></span> <strong>WATCH LIVE TV</strong></a>
							</div>
							<div className="lnlapp"><a href="/app-download/" target="_blank" rel="nofollow"><span className="nhapp-icon hsocial-sprite"></span><strong>DOWNLOAD News18 APP</strong></a>
							</div>
							<div className="nhsocial">
								<strong>Follow Us On</strong>
								<a href="https://www.facebook.com/cnnnews18/" className="nhfb-icon hsocial-sprite"></a>
								<a href="https://twitter.com/CNNnews18" className="nhtw-icon hsocial-sprite"></a>
								<a href="https://www.instagram.com/cnnnews18/" className="nhig-icon hsocial-sprite"></a>
								<a href="https://www.youtube.com/user/ibnlive" className="nhutb-icon hsocial-sprite"></a>
							</div>
						</div>
						<div className="nhtranding">
							Trending Topics :
							<a href="https://www.news18.com/newstopics/rajasthan.html" target="_self">#RajasthanCrisis </a>
							<a href="https://news18.com/news/career/board-results/ " target="_self">#BoardResults</a>
							<a href="/cricketnext/" target="_self"><strong>#Cricket</strong></a>
							<a href="/coronavirus-latest-news/" target="_self">Coronavirus</a>
						</div>
					</div>
				</div>
			</div>
			<Menul1/>
		</div>
	)
}

// const Link = ({ children, href }) => {
//   	const router = useRouter()
//   	return (
// 		<a href="#" onClick={(e) => {
// 				e.preventDefault()
// 				router.push(href)
// 			}
// 		}>
// 			{children}
// 			<style jsx>{`
// 				a {
// 				margin-right: 10px;
// 				}
// 			`}</style>
// 		</a>
//   	)
// }

export default Header;