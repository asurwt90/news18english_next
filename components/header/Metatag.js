const Metatag = (props) => (
        <>  
                <title> {props.title}</title>
                <meta name="description" content={props.title}/>
                <meta name="keywords" content={props.title}/>
                <link rel="icon" href="https://images.news18.com/static_news18/pix/ibnhome/news18/favicon.ico"/>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </>
)

export default Metatag;