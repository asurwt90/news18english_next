export default function menu_l1(){
	return(
		<nav>
			<div className="outerone clearfix">
				<a href="/" className="stky-logo fleft sroll-hide"><img src="https://images.news18.com/static_news18/pix/ibnhome/news18/megamenu/News18_logo.svg" alt="News18 Logo"/></a>		
				<ul className="subnav fright menu_tab">
					<li><a href="#"> <em></em></a></li>			
				</ul>
				<ul className="fleft nav-box">        
					<li><a href="/">Home</a></li>
					<li><a href="/coronavirus-latest-news/"><strong>Coronavirus</strong></a></li>
					<li><a href="/politics/">Politics</a></li>
					<li><a href="/india/">India</a></li>
					<li><a href="/entertainment/">Entertainment</a></li>
					<li><a href="/tech/">Tech</a></li>
					<li><a href="/auto/">Auto</a></li>
					<li><a href="/buzz/">Buzz</a></li>
					<li><a href="/videos/">Videos</a></li>
					<li><a href="/photogallery/" >Photos</a></li>
					<li><a href="/health-and-fitness/" >Health </a></li>			
					<li className="entrypoint-covid"><a href="#">Tracker</a></li>
				</ul>
				<div className="gsearch-box"></div>
				<ul className="header-search fright clearfix">			
					<li><a href="" className="search-icon spriteimage1 sbtn"></a></li>			
				</ul>
				<div className="nav-rgt-inside-links">
					<div className="social-links"> </div>
					<div className="sub-nav clearfix">
						<ul className="h-links clearfix">
							<li><a href="/" >Home</a></li>
							<li><a href="/coronavirus-latest-news/" ><strong >Coronavirus</strong></a></li>
							<li><a href="/politics/" >Politics</a></li>
							<li><a href="/india/">India</a></li>
							<li><a href="/entertainment/">Entertainment</a></li>
							<li><a href="/tech/">Tech</a></li>
							<li><a href="/auto/">Auto</a></li>
							<li><a href="/buzz/">Buzz</a></li>
							<li><a href="/videos/">Videos</a></li>
							<li><a href="/photogallery/">Photos</a></li>
							<li><a href="/health-and-fitness/">Health </a></li>
							<li><a href="https://www.squareyards.com/?utm_source=news18&amp;utm_medium=sqybanner&amp;utm_campaign=branding" rel="nofollow">Real Estate</a></li>
							<li><a href="/business/">Business</a></li>
							<li><a href="/world/">World</a></li>
							<li><a href="/education-career/">Education and Career</a></li>
							<li><a href="/astrology/">Astrology</a></li>
							<li><a href="/stocks/indian-stocks-market-live/">Stocks</a></li>
							<li><a href="/opinion/">Opinion</a></li>
							<li><a href="/lifestyle/">Lifestyle</a></li>
							<li><a href="/sports/">Sports</a></li>
							<li><a href="/podcast/">Podcast</a></li>
							<li><a href="/travel/">Travel</a></li>
							<li><a href="/football/">Football</a></li>
							<li><a href="/food/">Food</a></li>
							<li><a href="/cricketnext/">CricketNext</a></li>
							<li><a href="/mission-paani/"><span >&nbsp;</span></a></li>
						</ul>
						<div className="b-link clearfix">
							<a href="/news/">Latest</a>
							<a href="/movies/">Movies</a>
							<a href="/lifestyle/">Lifestyle </a>
							<a href="https://www.squareyards.com/?utm_source=news18&amp;utm_medium=sqybanner&amp;utm_campaign=branding" rel="nofollow">Real Estate</a>
							<a href="/education-career/">Education and Career</a>
							<a href="https://www.news18.com/indiapositive/">IndiaPositive</a>
							<a href="/icici-lombard-restart-right/" target="_blank" className="act">#RestartRight</a>	
						</div>
					</div>
				</div>
			</div>
		</nav>
	)
}