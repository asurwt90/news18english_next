//import { useRouter } from 'next/router'
import Link from 'next/link'
import AdsCode from '../dpf/adsCode';
import {customize_url} from '../utils/uitility';

function indexPagetpl(props) {
    return(
        <div id="left">
            <div className="flex-box justify-content">
                <div className="lead-story">
                    <figure className="lstory-top">
                        <Link as={customize_url(props.posts[0].url)} href={customize_url(props.posts[0].url)}>
                            <a>
                            <img src={props.posts[0].image} 
                            width="542" height="360" 
                            alt={props.posts[0].headline} 
                            title={props.posts[0].headline}/>
                            </a>
                        </Link>
                        <figcaption>
                            {/*<URLMasking weburl={props.posts[0].url} artitle_id={props.posts[0].id}></URLMasking>*/}
                            <Link href={customize_url(props.posts[0].url)}>
                            {/* <Link href={props.posts[0].url}> */}
                                <a>
                                    <span>{props.posts[0].poll_id}</span>
                                    <h1>  {props.posts[0].headline}</h1>
                                </a>
                            </Link>
                        </figcaption>
                    </figure>				
                    <ul className="lstory-btm">
                        <li>
                            <figure className="prelative">
                                <a href={props.posts[1].weburl}>
                                    <img className="" src={props.posts[1].image} width="262" height="173" data-src={props.posts[0].image} width="262" height="173 1x"/>
                                </a>
                            </figure>
                            <span className="font-family">{props.posts[1].poll_id}</span>
                            <p>
                                <a href={props.posts[1].weburl}>{props.posts[1].headline}</a>
                            </p>
                        </li>
                        <li>
                            <figure className="prelative">
                                <a href={props.posts[1].weburl}>
                                    <img src={props.posts[2].image} width="262" height="173 1x"/>
                                </a>
                            </figure>
                            <span className="font-family">{props.posts[2].poll_id}</span>
                            <p>
                                <a href={props.posts[2].weburl}>{props.posts[2].headline}</a>
                            </p>
                        </li>
                    </ul>
                </div>
                <ul className="lead-mstory">
                    {props.posts.map(storydetails => (
                        <li><a href={storydetails.weburl}>{storydetails.headline}</a></li>
                    ))}
                </ul>
            </div>
            <AdsCode 
				ads_div = ''
				ads_code = "BTF_728"
				ads_class = ""
			/>
        </div>
    )
}

export default indexPagetpl;