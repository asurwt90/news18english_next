import Link from 'next/link'

function articlePagetpl(props){
	return(
		<div id="left" className="articleleft">
			<div className="article-box">
				<h1>{props.posts.headline}</h1>
				<div className="article-byno-limg">
					<div className="article-bimg">
						<figure>
							<img src={props.posts.images.url} width="536" height="356"/>
						</figure>
						<p className="caption">{props.posts.headline}</p>						
					</div>
										
					<div className="article-bnow-box">
						<h2>{props.posts.intro}</h2>
						<ul className="article-bnow">
							<li><strong><a href="https://www.news18.com/agency/news18.com.html" className="agnam">News18.com</a></strong>
							</li>
							<li><strong>Last Updated:</strong> {props.posts.creation_date}</li>
						</ul>
						<div className="article-share">
							Share this: <a href="https://www.facebook.com/sharer.php?u=https://www.news18.com/news/politics/no-action-against-sachin-pilot-camp-for-now-as-rajasthan-hc-orders-status-quo-till-monday-2731467.html&amp;t=No Action Against Sachin Pilot Camp for Now as Rajasthan HC Orders ‘Status Quo’ Till Monday" className="fbicon article-sprite" target="_blank"></a><a href="https://twitter.com/share?text=No Action Against Sachin Pilot Camp for Now as Rajasthan HC Orders ‘Status Quo’ Till Monday&amp;url=https://www.news18.com/news/politics/no-action-against-sachin-pilot-camp-for-now-as-rajasthan-hc-orders-status-quo-till-monday-2731467.html" className="twicon article-sprite" target="_blank"></a><a href="https://web.whatsapp.com/send?text=No Action Against Sachin Pilot Camp for Now as Rajasthan HC Orders ‘Status Quo’ Till Monday - https://www.news18.com/news/politics/no-action-against-sachin-pilot-camp-for-now-as-rajasthan-hc-orders-status-quo-till-monday-2731467.html" target="_blank" className="article-sprite whatsapp"></a><a href="https://getpocket.com/edit?url=https://www.news18.com/news/politics/no-action-against-sachin-pilot-camp-for-now-as-rajasthan-hc-orders-status-quo-till-monday-2731467.html" target="_blank" className="article-sprite waicon"></a>							
						</div>
					</div>
				</div>
				<article className="article-content-box first_big_character" id="count_13">
					<p>{props.posts.body}</p>
				</article>
			</div>    
		</div>
	)
}

export default articlePagetpl;