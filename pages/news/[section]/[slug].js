import React from 'react'
// import { useRouter } from 'next/router'
import axios from 'axios'
import Layout from '../../../components/layout/common_layout'
// import { route } from 'next/dist/next-server/server/router'
// import fetch from 'isomorphic-fetch'
import ArticlePagetpl from '../../../components/templates/article.tpl'

const getArticlePage =(props) =>{
    // const router = useRouter();
    // console.log(router);
    // console.log(props.posts);
    return(
        <Layout title="Article page news">
            <ArticlePagetpl posts={props.posts}/>
        </Layout>
    )
}
getArticlePage.getInitialProps = async (props) => {
    try {
        var slug = props.req.url;
        var lastIndexOfDot = slug.lastIndexOf('.');
        var lastIndexOfDash = slug.lastIndexOf('-');
        var slugWoExt = slug.substr(0, lastIndexOfDot);
        const article_id = slugWoExt.substr((lastIndexOfDash+1), (slugWoExt.length-lastIndexOfDash));

        const article_data= await axios.get("http://api.news18.com/en/get_redis/story_"+article_id);
        return { 
            posts: article_data.data 
        };
    }catch (error) {
		return { posts:{}};
	}   
}

export default getArticlePage;

/* export async function getServerSideProps(context) {
    const res = await fetch(
        "https://static-next.willemliu.now.sh/api/test"
    ).then((res) => res.json());
    console.log("getServerProps", res, context.params, context.query);
    return {
      //  props: { ...res, name: context.query.slug }
    };
}*/

/* function getStoryById() {
    const router = useRouter()
    console.log(router.query.id);
    var article_id=router.query.id;
    const {  data: result, error  } = useSWR('https://cloudapi.nw18.com/global/getRedisData.php?app=IBNLIVE&key=story_'+article_id, fetcher)
    console.log(result)
    
    if (error) return <h1>Something went wrong!</h1>
    
    if (!result) return <h1>Loading...</h1>
    return (
        <main className='App'>
            <h1>Pokedex</h1>
            <div>
                {result.results.map((detauils) => (
                    <div>{detauils.name}</div>
                ))}
            </div>
        </main>
    )
}*/