import Layout from '../components/layout/common_layout'
// import Link from 'next/link'
import axios from 'axios'
// import HotTrending from '../components/homepage_hot_trending'
import IndexPagetpl from '../components/templates/index.tpl'
// import {testbyme} from '../components/utils/uitility'

const HomePage = (props) =>{
	const newStoryData = [];
	for (let i = 3; i < 13; i++) {
		if(props.posts[i]!=''){
		newStoryData.push(props.posts[i])
		}
	}
	
	return (
		<Layout title="Home page news">
			{newStoryData.length > 0 &&
				<IndexPagetpl posts={newStoryData}/>
			}
		</Layout>
	)
}

HomePage.getInitialProps = async (req,res) => {
	try {
		const postss = await axios.get("https://cloudapi.nw18.com/global/getRedisData.php?app=IBNLIVE&key=hometop5stories");
  		return { posts: postss.data };
	} catch (error) {
		return { posts:{}};
	}
}

export default HomePage;